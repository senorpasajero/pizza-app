var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

var DIST_DIR   = path.join(__dirname, "dist"),
    ASSETS_DIR = path.join(__dirname, "src", "assets"),
    CLIENT_DIR = path.join(__dirname, "src", "client");

module.exports = {
  context: CLIENT_DIR,
  devtool: "source-map",
  entry: {
    main: [
      "webpack/hot/dev-server",
      "webpack-hot-middleware/client",
      "./main.ts"
    ],
    polyfills: "./polyfills.js"
  },
  output: {
    path: path.join(DIST_DIR, 'js'),
    publicPath: "http://localhost:3000/js/",
    filename: "[name].js"
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: [[ "es2015", { "modules": false }]]
          }
        },
        { loader: "ts-loader" }
      ]
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: [[ "es2015", { "modules": false }]]
          }
        }
      ]
    }, {
      test: /\.scss$/,
      use: ExtractTextPlugin.extract('css-loader!sass-loader')
    }]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    modules: [
      'node_modules'
    ]
  },
  plugins:  [
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([
        { from: './**/*.html', to: DIST_DIR }
    ]),
    new ExtractTextPlugin('../css/style.css')
  ]
};