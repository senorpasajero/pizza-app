dependencies:
    node v8
    npm 5
    mysql 14

Before running the app

1. npm install -g webpack sequelize-cli

2. Create MySql User:
    user: root
    password: 

3. sequelize db:create
4. sequelize db:migrate
5. sequelize db:seed:all

Running the app

1. mysql.server start
2. npm install
3. npm run build
3. npm start

Visit: http://localhost:3000/

API

1. GET /api returns all orders
2. GET /api/:id returns an order
3. PUT /api/:id update an order
4. DELETE /api/:id delete an order
