import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { IndexService } from './components/index/index.service';
import { IndexComponent } from './components/index/index.component';
import { DetailsComponent } from './components/details/details.component';
import { DetailsService } from './components/details/details.service';
import { EditComponent } from './components/edit/edit.component';
import { EditResolve } from './components/edit/edit.resolve';
import { EditService } from './components/edit/edit.service';
import { NewComponent } from './components/new/new.component';
import { NewService } from './components/new/new.service';
import { OrdersService } from './services/orders.service';
import { AppComponent } from './components/app.component';

require('../sass/app.scss');

@NgModule({
  declarations: [
    IndexComponent,
    DetailsComponent,
    EditComponent,
    NewComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([{
      path: '',
      redirectTo: '/orders', 
      pathMatch: 'full'
    }, {
      path: 'orders/new',
      component: NewComponent,
      data: { }
    }, {
      path: 'orders/:id',
      component: DetailsComponent,
      data: { }
    }, {
      path: 'orders',
      component: IndexComponent,
      data: { }
    }, {
      path: 'orders/:id/edit',
      component: EditComponent,
      resolve: {
        order: EditResolve
      },
      data: { }
    }], {
      enableTracing: true
    })
  ],
  providers: [
    IndexService,
    DetailsService,
    EditResolve,
    EditService,
    NewService,
    OrdersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
