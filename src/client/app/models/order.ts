export class Order {
    constructor(
        public client: string,
        public address: string,
        public flavor: string,
        public phone: string,
        public id?: number
    ) { }
}