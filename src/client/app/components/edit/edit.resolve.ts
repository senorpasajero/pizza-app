import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { IOrder } from '../../interfaces/order.interface';
import { OrdersService } from '../../services/orders.service';

@Injectable()
export class EditResolve implements Resolve<IOrder> {

    constructor(
        private ordersService: OrdersService
    ) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.ordersService.getOrder(Number(route.paramMap.get('id')));
    }
}