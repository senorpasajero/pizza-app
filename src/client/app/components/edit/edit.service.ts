import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOrder } from '../../interfaces/order.interface';

@Injectable()
export class EditService {
    constructor(
        private http: HttpClient
    ) {}

    updateOrder(order: IOrder) {
        return this.http.put<IOrder[]>(`/api/${order.id}`, { 'order': order});
    }
}