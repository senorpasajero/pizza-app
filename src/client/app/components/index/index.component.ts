import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IOrder } from '../../interfaces/order.interface';
import { IndexService } from './index.service';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'my-app-index',
  template: `
    <h1>Order's Control Panel</h1>
    <div class="new-order">
        <a [routerLink]="['/orders', 'new']">New Order</a>
    </div>
    <div class="orders">
        <div class="order" *ngFor="let order of orders">
            <h2 (click)="onDetail(order)">{{ order.client }}</h2>
        </div>
    </div>
    `
})
export class IndexComponent implements OnInit { 
    name = 'Bob';
    orders: IOrder[];

    constructor(
        private router: Router,
        private indexService: IndexService,
        private ordersService: OrdersService
    ) {}

    ngOnInit(): void {
        this.indexService.getOrders().subscribe(data => {
            this.orders = data;
            this.ordersService.setOrders(this.orders);
        });
    }

    onDetail(order: IOrder) {
        this.router.navigate(['/orders', order.id]);
    }
}
