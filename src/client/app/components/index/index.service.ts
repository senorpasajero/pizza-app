import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOrder } from '../../interfaces/order.interface';

@Injectable()
export class IndexService {
    constructor(
        private http: HttpClient
    ) {}

    getOrders() {
        return this.http.get<IOrder[]>('/api');
    }
}