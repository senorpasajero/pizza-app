import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DetailsService } from './details.service';
import { IOrder } from '../../interfaces/order.interface';

@Component({
  selector: 'my-app-details',
  template: `
    <h1>{{order?.client}}</h1>
    <h2>{{order?.address}}</h2>
    <h3>{{order?.phone}}</h3>
    <h4>{{order?.flavor}}</h4>
    <button [routerLink]="['/orders']">Back</button>
    <button (click)="onEdit(order)">Edit</button>
    <button (click)="onDelete(order)">Delete</button>
    `
})
export class DetailsComponent implements OnInit{ 
    name = 'Bob';
    order: IOrder;
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private detailsService: DetailsService
    ) {}
    ngOnInit(): void {
        this.detailsService.getOrder(this.route.snapshot.paramMap.get('id')).subscribe((data) => {
            this.order = data;
        });
    }

    onEdit(order: IOrder) {
        this.router.navigate(['/orders', order.id, 'edit']);
    }

    onDelete(order: IOrder) {
        this.detailsService.deleteOrder(String(order.id)).subscribe(data => {
            this.router.navigate(['/orders']);
        });
    }
}
