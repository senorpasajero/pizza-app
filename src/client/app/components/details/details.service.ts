import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IOrder } from '../../interfaces/order.interface';

@Injectable()
export class DetailsService {
    constructor(
        private http: HttpClient
    ) {}

    getOrder(id: string) {
        return this.http.get<IOrder>(`/api/${id}`)
    }

    deleteOrder(id: string) {
        return this.http.delete<any>(`/api/${id}`);
    }
}