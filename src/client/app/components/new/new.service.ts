import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOrder } from '../../interfaces/order.interface';

@Injectable()
export class NewService {
    constructor(
        private http: HttpClient
    ) {}

    newOrder(order: IOrder) {
        return this.http.post<IOrder[]>(`/api`, { 'order': order});
    }
}