import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IOrder } from '../../interfaces/order.interface';
import { Order } from '../../models/order';
import { NewService } from './new.service';

@Component({
  selector: 'my-app-edit',
  template: `
    <h1>Edit Order: {{ model?.id}}</h1>
    <form (ngSubmit)="onSubmit()" #orderForm="ngForm">
        <div class="form-group">
            <label for="client">Client:</label>
            <input type="text" class="form-control" [(ngModel)]="model.client" name="client" id="client" required>
        </div>
        <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" class="form-control" [(ngModel)]="model.address" name="address" id="address" required>
        </div>
        <div class="form-group">
            <label for="flavor">Flavor:</label>
            <select class="form-control" [(ngModel)]="model.flavor" name="flavor" id="flavor" required>
                <option *ngFor="let flavor of flavors" [value]="flavor">{{flavor}}</option>
            </select>
        </div>
        <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="text" class="form-control" [(ngModel)]="model.phone" name="phone" id="phone" required>
        </div>

        <button [disabled]="!orderForm.form.valid" type="submit" class="btn btn-success">Submit</button>
    </form>
    `
})
export class NewComponent implements OnInit {
    flavors: String[] = ['hawaian', 'hawaian 2', 'hawaian 3', 'hawaian 4', 'hawaian 5'];
    model: Order;

    constructor(
        private router: Router,
        private newService: NewService
    ) {}

    ngOnInit(): void {
        this.model = new Order('', '', '', '');
    }

    onSubmit(order: IOrder) {
        this.newService.newOrder({
            client: this.model.client,
            address: this.model.address,
            phone: this.model.phone,
            flavor: this.model.flavor
        }).subscribe((data) => {
            this.router.navigate(['/orders']);
        });
    }
}
