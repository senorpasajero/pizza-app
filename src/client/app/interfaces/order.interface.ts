export interface IOrder {
    id?: number;
    client: string;
    flavor: string;
    address: string;
    phone: string;
    created_at?: Date;
    updated_at?: Date;
};
