import { Injectable } from '@angular/core';
import { IOrder } from '../interfaces/order.interface';

@Injectable()
export class OrdersService {
    private _orders: IOrder[];

    setOrders(orders: IOrder[]): void {
        this._orders = orders;
    }

    getOrder(id: number): IOrder {
        return this._orders.filter(order => order.id === id)[0];
    }

    getOrders(): IOrder[] {
        return this._orders;
    }
}
