const db = require('../models');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

module.exports = function(router) {
    router.get('/', function(req, res) {
        db.order.findAll().then((orders) => {
            res.status(200).json(orders);
        });
    });

    router.get('/:id', function(req, res) {
        db.order.find({
            where: {
                id: req.params.id
            }
        }).then((order) => {
            res.status(200).json(order);
        });
    });

    router.post('/', bodyParser.json(), function(req, res) {
        db.order.create(req.body.order).then((order) => {
            res.json(order);
        });
    });

    router.put('/:id', bodyParser.json(), function(req, res) {
        console.log(req.body);
        db.order.update(
            req.body.order,
            {
            where: {
                id: req.params.id
            }
        }).then((order) => {
            res.status(200).json(order);
        });
    });

    router.delete('/:id', function(req, res) {
        db.order.destroy({
            where: {
                id: req.params.id
            }
        }).then(order => {
            res.json(order).status(200);
        });
    });

    return router;
};