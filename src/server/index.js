import webpack from "webpack";
import express from "express";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import db from "../src/server/models";
import config from "../webpack.config.js";


const DEFAULT_PORT = 3000;
const app = express();
const router = express.Router();
const api = require('../src/server/routes/orders')(router);
const compiler = webpack(config);

app.use('/api', api);

app.use('/', express.static(__dirname + '/'));

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: {colors: true}
}));

app.use(webpackHotMiddleware(compiler, {
    log: console.log
}));

app.listen(process.env.PORT || DEFAULT_PORT, function () {
  console.log('Pizza app listening on port 3000!');
});
