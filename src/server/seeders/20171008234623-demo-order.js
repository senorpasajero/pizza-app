'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Orders', [{
        client: 'John Doe',
        address: 'Av. John Doe 1601',
        phone: "555 55 55",
        flavor: "hawaian"
      }, {
        client: 'John Doe 2',
        address: 'Av. John Doe 1601',
        phone: "555 55 55",
        flavor: "hawaian 2"
      }, {
        client: 'John Doe3',
        address: 'Av. John Doe 1601',
        phone: "555 55 55",
        flavor: "hawaian 3"
      }, {
        client: 'John Doe 4',
        address: 'Av. John Doe 1601',
        phone: "555 55 55",
        flavor: "hawaian 4"
      }, {
        client: 'John Doe5',
        address: 'Av. John Doe 1601',
        phone: "555 55 55",
        flavor: "hawaian 5"
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('Orders', null, {});
  }
};
